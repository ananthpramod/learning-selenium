package week5.day1;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reporting {
	public static void main(String[]args) throws IOException {
		

	ExtentHtmlReporter html=new ExtentHtmlReporter("./reports/test1.html");
	ExtentReports extent= new ExtentReports();
	extent.attachReporter(html);
	ExtentTest test= extent.createTest("createlead", "Test");
	test.assignAuthor("me");
	test.assignCategory("Sample");
	test.pass("step:1 passed", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img.png").build());
	
	test.fail("step:2 passed", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img.png").build());
	
	test.fail("step:3 passed", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img.png").build());
	
	test.fail("step:4 passed", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img.png").build());
	
	test.fail("step:5 passed", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img.png").build());
	
	extent.flush();
}}
