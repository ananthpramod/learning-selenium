
package class6.day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LearningExceldata {


	public  static String [][] main() throws IOException {



		XSSFWorkbook book = new XSSFWorkbook("./TestData/Create_Lead.xlsx");
		XSSFSheet sheet = book.getSheetAt(0);

		int rowcount=sheet.getLastRowNum();
		int colcount=sheet.getRow(0).getLastCellNum();

		System.out.println("Row Count:"+rowcount);
		System.out.println("Col Count:"+colcount);
		String[][] data=new String[rowcount][colcount];

		for (int i = 1; i <= rowcount; i++) {
			XSSFRow row=sheet.getRow(i);
			for (int j = 0; j < colcount; j++) {
				XSSFCell cell=row.getCell(j);

				String cellvalue= cell.getStringCellValue();
				System.out.println(cellvalue);
				data[i-1][j]= cellvalue;

			}

		}
		return data;



	}

}
