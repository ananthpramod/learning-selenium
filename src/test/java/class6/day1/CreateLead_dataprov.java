package class6.day1;

import wdmethods.SeMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import class6.day2.LearningExceldata;
import wdmethods.ProjectMethods;

public class CreateLead_dataprov extends ProjectMethods {

	@AfterMethod
	public void close() {
		closeAllBrowsers();
	}

	@BeforeClass
	@Parameters({"bro","url","login","pawd"})
	public void  login1(String bro, String url,String login,String pawd)  {



		/*bro="Chrome";
			url=;
			login=;
			pawd=;
			System.out.println("bef af");
			startApp(bro, ur5*/
		System.out.println("print af");

	}

	@Test(dataProvider="fetchData")
	public void CreateLeads(String cname,String fname,String lname ) {


		WebElement eleCRMSFA = locateElement("xpath", "//a[contains(text(),'CRM/SFA')]");
		click (eleCRMSFA);
		WebElement elecreatelead = locateElement("xpath", "//a[contains(text(),'Create Lead')]");
		click (elecreatelead);
		WebElement eleCmpnyName = locateElement("id", "createLeadForm_companyName");
		type(eleCmpnyName,cname);
		WebElement eleFirstname = locateElement("xpath", "//input[@id='createLeadForm_firstName']");
		type(eleFirstname,fname);
		WebElement eleLastname = locateElement("xpath", "//input[@id='createLeadForm_lastName']");
		type(eleLastname,lname);

		WebElement eleSource = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(eleSource, "Partner","");

		/*WebElement eleMarket = locateElement("id", "createLeadForm_marketingCampaignId");
			selectDropDownUsingIndex(eleMarket, 4);*/

		/*
			WebElement eleFnameL = locateElement("id", "createLeadForm_firstNameLocal");
			type(eleFnameL, "Abdul");
			WebElement eleLnameL = locateElement("id", "createLeadForm_lastNameLocal");
			type(eleLnameL, "Kalam");

			WebElement eleSalutation = locateElement("id", "createLeadForm_personalTitle");
			type(eleSalutation, "Mr.");

			WebElement eleDOB = locateElement("id", "createLeadForm_birthDate");
			type(eleDOB, "1/1/90");
		 */



		WebElement eleSubmit = locateElement("name", "submitButton");
		click (eleSubmit);
	}	

	@DataProvider(name="fetchData")
	public String[][]  getexdata() throws IOException {

		 
		return LearningExceldata.main();
		
		/*
		data[0] [0]="comp name 0";
		data[0] [1]="f name 0";
		data[0] [2]="l name 0";
		data[1] [0]="comp name 1";
		data[1] [1]="f name 1";
		data[1] [2]="l name 1";*/



	}






}