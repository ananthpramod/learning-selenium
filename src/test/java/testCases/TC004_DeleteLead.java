package testCases;

import org.testng.annotations.Test;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import wdmethods.ProjectMethods;
//import wdmethods.SeMethods_old;


public class TC004_DeleteLead extends ProjectMethods{

	@BeforeClass(groups="test.smoke")	
	public void loginto(String bro,String url,String login,String pawd) {
		//startApp("chrome", "http://leaftaps.com/opentaps");
		login(bro,url,login,pawd);
	}
	@AfterMethod
	public void close() {
		closeAllBrowsers();
	}
	@Test(priority=3)
	public void DeleteLead() throws InterruptedException{
		
	
		
		WebElement lnkCRM = locateElement("linktext", "CRM/SFA");
		click(lnkCRM);
		
		WebElement lnkLead = locateElement("linktext", "Leads");
		click(lnkLead);
		
		WebElement lnkFLead = locateElement("linktext", "Find Leads");
		click(lnkFLead);
		
		WebElement phone = locateElement("xpath", "//span[text()='Phone']");
		click(phone);
		
		WebElement arCode = locateElement("name", "phoneAreaCode");
		type(arCode, "302");
		
		WebElement phNumber = locateElement("name", "phoneNumber");
		type(phNumber, "3323574");
		
		WebElement finLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(finLeads);
		
		WebElement lnksearch = locateElement("xpath", "//table[@class='x-grid3-row-table']//a");
		String searchid = getText(lnksearch);
		System.out.println(searchid);
		click(lnksearch);
		
		WebElement butDel = locateElement("class", "subMenuButtonDangerous");
		click(butDel);
		
		Thread.sleep(2000);
		
		WebElement lnk1FLead = locateElement("linktext", "Find Leads");
		click(lnk1FLead);
		
		WebElement txtId = locateElement("name", "id");
		type(txtId, searchid);
		
		WebElement fin1Leads = locateElement("xpath", "//button[text()='Find Leads']");
		click(fin1Leads);
		
		WebElement error = locateElement("xpath", "//div[@class='x-paging-info']");
		
		System.out.println(getText(error));
		
	}

}
