package testCases;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;


public class TC001_Login extends ProjectMethods{

	@AfterMethod
	public void close() {
		closeAllBrowsers();
	}

	@Test(priority=5)
	public void CreateLeads() {


		WebElement eleCRMSFA = locateElement("xpath", "//a[contains(text(),'CRM/SFA')]");
		click (eleCRMSFA);
		WebElement elecreatelead = locateElement("xpath", "//a[contains(text(),'Create Lead')]");
		click (elecreatelead);
		WebElement eleCmpnyName = locateElement("id", "createLeadForm_companyName");
		type(eleCmpnyName,"Wipro");
		WebElement eleFirstname = locateElement("xpath", "//input[@id='createLeadForm_firstName']");
		type(eleFirstname, "Abdul");
		WebElement eleLastname = locateElement("xpath", "//input[@id='createLeadForm_lastName']");
		type(eleLastname, "Kalam");

		WebElement eleSource = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(eleSource, "Partner","");

		/*WebElement eleMarket = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingIndex(eleMarket, 4);*/


		WebElement eleFnameL = locateElement("id", "createLeadForm_firstNameLocal");
		type(eleFnameL, "Abdul");
		WebElement eleLnameL = locateElement("id", "createLeadForm_lastNameLocal");
		type(eleLnameL, "Kalam");

		WebElement eleSalutation = locateElement("id", "createLeadForm_personalTitle");
		type(eleSalutation, "Mr.");

		WebElement eleDOB = locateElement("id", "createLeadForm_birthDate");
		type(eleDOB, "1/1/90");




		WebElement eleSubmit = locateElement("name", "submitButton");
		click (eleSubmit);





	}

}









