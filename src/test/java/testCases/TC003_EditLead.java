package testCases;

import org.testng.annotations.Test;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import wdmethods.ProjectMethods;

public class TC003_EditLead extends ProjectMethods{

	@BeforeClass(groups="Test.Regression")		
	public void loginto(String bro,String url,String login,String pawd) {
		//startApp("chrome", "http://leaftaps.com/opentaps");
		login(bro,url,login,pawd);
	}
	
	@AfterMethod
	public void close() {
		closeAllBrowsers();
	}
	@Test(priority =4)
	public void EditLead(){

		//	login();
		WebElement lnkCRM = locateElement("linktext", "CRM/SFA");
		click(lnkCRM);

		WebElement lnkLead = locateElement("linktext", "Leads");
		click(lnkLead);

		WebElement lnkFLead = locateElement("linktext", "Find Leads");
		click(lnkFLead);

		WebElement txtFName = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(txtFName, "sangitha");

		WebElement BtFLead = locateElement("xpath", "//button[text()='Find Leads']");
		click(BtFLead);

		WebElement lnk = locateElement("xpath", "//table[@class='x-grid3-row-table']//a");
		click(lnk);

		WebElement butEdit = locateElement("linktext", "Edit");
		click(butEdit);

		/*WebElement EdtCName = locateElement("updateLeadForm_companyName");
		//typeAppend(EdtCName, " Technology Solutions");
*/
		WebElement butSubmit = locateElement("xpath", "(//input[@class='smallSubmit'])[1]");
		click(butSubmit);
	}

}
