package testCases;

import org.testng.annotations.Test;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import wdmethods.ProjectMethods;

//import wdmethods.SeMethods_old;

public class TC005_DuplicateLead extends ProjectMethods {
	
	@BeforeClass(groups="test.sanity",dependsOnGroups="test.Regression")	
	public void loginto(String bro,String url,String login,String pawd) {
		//startApp("chrome", "http://leaftaps.com/opentaps");
		login(bro,url,login,pawd);
	}
		
	
	@AfterClass
	public void close() {
		
	}
	@Test(priority=1)
	public void duplicateLead() {
	
		WebElement lnkCRM = locateElement("linktext", "CRM/SFA");
		click(lnkCRM);
		
		WebElement lnkLead = locateElement("linktext", "Leads");
		click(lnkLead);
		
		WebElement lnkFLead = locateElement("linktext", "Find Leads");
		click(lnkFLead);
		
		WebElement lnkemail = locateElement("linktext", "Email");
		click(lnkemail);
		
		WebElement txtemail = locateElement("name", "emailAddress");
		type(txtemail, "sangitha.chennai@gmail.com");
		
		WebElement FLead = locateElement("xpath", "//button[text()='Find Leads']");
		click(FLead);
		
		WebElement search = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-firstName']/a");
		String searchtxt = getText(search);
		click(search);
		
		WebElement dupLead = locateElement("linktext", "Duplicate Lead");
		click(dupLead);
		
		verifyTitle("Duplicate Lead | opentaps CRM");
		
		WebElement butCreate = locateElement("class", "smallSubmit");
		click(butCreate);
		
		WebElement viewFName = locateElement("viewLead_firstName_sp");
		
		verifyExactText(viewFName, searchtxt);
			
		closeBrowser();
		
	}

}
