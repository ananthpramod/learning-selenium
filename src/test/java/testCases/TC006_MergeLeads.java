package testCases;

import org.testng.annotations.Test;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import wdmethods.ProjectMethods;

public class TC006_MergeLeads extends ProjectMethods {

	@BeforeClass(groups="test.smoke")	
	public void loginto(String bro,String url,String login,String pawd) {
		//startApp("chrome", "http://leaftaps.com/opentaps");
		login(bro,url,login,pawd);
	}
	@AfterMethod
	public void close() {
	//	closeAllBrowsers();
	}
	@Test(priority=2)
	public void MergeLeads() throws InterruptedException {
		
		
		WebElement lnkCRM = locateElement("linktext", "CRM/SFA");
		click(lnkCRM);
		
		WebElement lnkLead = locateElement("linktext", "Leads");
		click(lnkLead);
		
		WebElement lnkMerge = locateElement("linktext", "Merge Leads");
		click(lnkMerge);
		
	/*	WebElement from = locateElement("xpath", "//table[@name='ComboBox_partyIdFrom']/following::img");*/
		WebElement from = locateElement("xpath", "//img[@alt='Lookup']");
		click(from);
		
		switchToWindow(1);
		
		WebElement newid = locateElement("name", "id");
		type(newid, "10004");
		
		WebElement butMFLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(butMFLeads);
		
		Thread.sleep(2000);
		WebElement Fselect = locateElement("linktext", "10004");
		click(Fselect);
		
		
	}
}
