package week3.day1;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Login {

	public static void main(String[] args) throws InterruptedException {
		// Chrome driver path
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	//	System.setProperty("webdriver.firefox.driver", "./drivers/geckodriver.exe");
		
	ChromeDriver driver=new ChromeDriver();
	
		
		
		driver.get("http://leaftaps.com/opentaps");
		
		
		WebElement userName=driver.findElementById("username");
		userName.sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		
		driver.findElementByClassName("decorativeSubmit").click();
		
		
		driver.findElementByLinkText("CRM/SFA").click();
		
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("AAA");
		driver.findElementById("createLeadForm_firstName").sendKeys("testa");
		driver.findElementById("createLeadForm_lastName").sendKeys("testb");
		
		
		WebElement source= driver.findElementById("createLeadForm_dataSourceId");
		Select se =new Select(source) ;
		
		se.selectByVisibleText("Employee");
//		driver.findElementById("createLeadForm_dataSourceId").sendKeys("Cold Call");
		
		WebElement source1= driver.findElementById("createLeadForm_marketingCampaignId");
		Select se1 =new Select(source1) ;
		
		se.selectByValue("CATRQ_CARNDRIVER");
		
		
//		
		
		driver.findElementByName("submitButton").click();
		
		driver.findElementById("createLeadForm_dataSourceId").click();
		
		WebElement source2= driver.findElementById("createLeadForm_industryEnumId");
		Select se2 =new Select(source2) ;
		
		se.selectByValue("CATRQ_CARNDRIVER");
	
		
		Thread.sleep(6000);
		driver.close();
		
		
		
		

	}

}
