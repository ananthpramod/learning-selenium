package week3.day2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Webtableverify {

	public static void main(String[] args) throws InterruptedException {
	
		 //Chrome driver path
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
					ChromeDriver driver =new ChromeDriver();
					
					//launch browser	
	driver.get("https://erail.in");
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	
	driver.findElementById("txtStationFrom").clear();
	driver.findElementById("txtStationFrom").sendKeys("MAS"+Keys.TAB);
	
	driver.findElementById("txtStationTo").clear();
	driver.findElementById("txtStationTo").sendKeys("sbc"+Keys.TAB);
	
	WebElement check = driver.findElementById("chkSelectDateOnly");

	if(check.isSelected()){
	check.click();	
	}
	
	driver.findElementByXPath("//*[contains(text(),'Train Name')]").click();
	
	
	WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
	

	
List<WebElement> rows= table.findElements(By.xpath("//table[@class='DataTable TrainList']/tbody/tr"));

System.out.println(rows.size());

for (int i = 0; i<rows.size(); i++) {
	WebElement eachrow= rows.get(i);
	
	List<WebElement> allcell= eachrow.findElements(By.tagName("td"));
	
	System.out.println(allcell.get(1).getText());
	
}}}