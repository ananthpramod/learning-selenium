package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ActionsHW {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver =new ChromeDriver();

		//launch browser	
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		
		WebElement userName=driver.findElementByXPath("//input[@id ='username']");
		userName.sendKeys("DemoSalesManager");
		
		//login pwd
		driver.findElementByXPath("//input[@id ='password']").sendKeys("crmsfa");
		
		//Click submit				
		
		driver.findElementByXPath("//input[@class='decorativeSubmit']").click();
		
		// Navigate to crm/sfa
		driver.findElementByXPath("//a[contains(text(),'CRM/SFA')]").click();
		
		driver.findElementByLinkText("Leads").click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		//from lead
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("//img[@src='/images/fieldlookup.gif'][1]").click();
		
		Set<String> Handle = driver.getWindowHandles();
		List <String> Li = new ArrayList<>();
		Li.addAll(Handle);
		driver.switchTo().window(Li.get(1));
		
		//to verify the control of new window
		System.out.println(driver.getTitle());
		
		driver.findElementByName("id").sendKeys("10116");
		
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		driver.findElementByLinkText("10116").click();
		
		driver.switchTo().window(Li.get(0));
		
		System.out.println(driver.getTitle());
		
		
		//to lead
		
		driver.findElementByXPath("//img[@src='/images/fieldlookup.gif'][2]").click();
		
driver.switchTo().window(Li.get(1));
		
		//to verify the control of new window
		System.out.println(driver.getTitle());
		
		driver.findElementByName("id").sendKeys("10124");
		
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		driver.findElementByLinkText("10124").click();
		
		driver.switchTo().window(Li.get(0));
		
		System.out.println(driver.getTitle());
		
		
		
		driver.findElementByLinkText("Merge").click();
		driver.switchTo().alert().accept();
		
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByName("id").sendKeys("10116");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		driver.quit();
		
		System.out.println("Execution is complete");	
		

	}

}
