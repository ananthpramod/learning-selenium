package week4.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class Frames {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver =new ChromeDriver();

		//launch browser	
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();
		driver.switchTo().alert().sendKeys("Aravindha");
		driver.switchTo().alert().accept();
		String text = driver.findElementById("demo").getText();
		if (text.contains("test"))
				System.out.println("Text exists");
		else
			System.out.println("Text does not exists");
		 
		driver.close();
			
				
	}

}