package week4.day1;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class CheckFrames {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver =new ChromeDriver();

		//launch browser	
		driver.get("https://irctc.co.in");
		driver.manage().window().maximize();
//		Thread.sleep(5000);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByXPath("//span[text()='AGENT LOGIN']").click();
		driver.findElementByXPath("//a[text()='Contact Us']").click();
		Set<String> Wh1 = driver.getWindowHandles();
		List <String> L1 = new ArrayList<>();
		L1.addAll(Wh1);
		driver.switchTo().window(L1.get(1));
		System.out.println(driver.getTitle());
		driver.quit();

	}}

