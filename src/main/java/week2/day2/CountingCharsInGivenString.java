package week2.day2;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;

public class CountingCharsInGivenString {

	public static void main(String[] args) {
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter String");
		String s=sc.next();
		
		char[] st = s.toCharArray();
		Map<Character, Integer> data=new TreeMap<>();
	
	
		
		for (char c : st) {
	if (data.containsKey(c)) {
		data.put(c, data.get(c)+1);
	}
	
	else {
		
		
		data.put(c, 1);
		
	}
	
		}
		
		
//	System.out.print(data);	
	for (Entry<Character, Integer> data1: data.entrySet()) {
		
		System.out.println(data1.getKey()+" --> "+data1.getValue());
		
		
	}	
		
		
		

	}

}
