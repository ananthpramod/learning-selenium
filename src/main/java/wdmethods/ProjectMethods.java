package wdmethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

public class ProjectMethods extends SeMethods {
@Parameters({"bro","url","login","pawd"})
@BeforeMethod
	public void  login(String bro,String url,String login,String pawd)  {

		startApp(bro, url);

		WebElement uname = locateElement("id", "username");
		type(uname, login);

		WebElement pwd = locateElement("password");
		type(pwd, pawd);

		WebElement sButton = locateElement("class", "decorativeSubmit");
		click(sButton);
	}
	
	

}
