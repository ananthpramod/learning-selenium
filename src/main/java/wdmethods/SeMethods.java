package wdmethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

public class SeMethods implements WdMethods {
	public RemoteWebDriver driver;
	int i =1;
	String filename;
	@Override
	public void startApp(String browser, String url) {
		if (browser.equalsIgnoreCase("chrome")) {
			//windows
			//System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver = new ChromeDriver();
		}else if(browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver");
			driver = new FirefoxDriver();
		}
		driver.manage().window().maximize();
		driver.get(url); 
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("The browser "+browser+" launched successfully"); 
		takeSnap();
	}
	
/*	@Override
	public void login() {
	
		WebElement userName=driver.findElementByXPath("//input[@id ='username']");
		userName.sendKeys("DemoSalesManager");
	
		driver.findElementByXPath("//input[@id ='password']").sendKeys("crmsfa");
	}*/

	@Override
	public WebElement locateElement(String locator, String locValue) {
		try {
			switch (locator) {
			case "id": return driver.findElementById(locValue);
			case "name": return driver.findElementByName(locValue);	
			case "xpath": return driver.findElementByXPath(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "linktext": return driver.findElementByLinkText(locValue);
			case "CssSelector": return driver.findElementByCssSelector(locValue);
			case "PartialLinkText":return driver.findElementByPartialLinkText(locValue);
			case "TagName": return driver.findElementByTagName(locValue);

			}
		} catch (NoSuchElementException e) {
			System.out.println("The element is not found");
		} catch (WebDriverException e) {
			System.out.println("Unknown Exception occured");
		} 
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		try {
			return driver.findElementById(locValue);
		} catch (NoSuchElementException e) {
			System.out.println("The element is not found");

		}
		catch (WebDriverException e) {
			System.out.println("Unknown Exception occured");
		} 
		return null;


	}

	@Override
	public void type(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data);
			System.out.println("The data "+data+" enter successfully");
		} catch (WebDriverException e) {
			System.out.println("The data "+data+" not enter successfully");
		} finally {
			takeSnap();
		}
	}

	/*@Override
	public void typeAppend(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			System.out.println("The data "+data+" enter successfully");
		} catch (WebDriverException e) {
			System.out.println("The data "+data+" not enter successfully");
		} finally {
			takeSnap();
		}
	}*/

	@Override
	public void click(WebElement ele) {
		ele.sendKeys(Keys.ENTER);
		System.out.println("The element "+ele+" clicked"); 
		takeSnap();
	}

	public void clickWoSnap(WebElement ele) {
		ele.click();
		System.out.println("The element "+ele+" clicked"); 

	}

	@Override
	public String getText(WebElement ele) {
		String text = ele.getText();
		System.out.println("The text: "+text+" is extracted successfully");
		return text;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value,String Sel) {

		Select dropdwn = new Select(ele);
		if (Sel.equalsIgnoreCase("visible")) {
			dropdwn.selectByVisibleText(value);

		}
		else if (Sel.equalsIgnoreCase("value")) {
			dropdwn.selectByValue(value);
		}
		else if (Sel.equalsIgnoreCase("index")) {
			dropdwn.selectByIndex(Integer.parseInt(value));	

		}

		System.out.println("The "+value+" selected successfully");
		takeSnap();
	}


	@Override
	public boolean verifyTitle(String expectedTitle) {
		boolean breturn = false;
		String title = driver.getTitle();
		if(title.equalsIgnoreCase(expectedTitle)) {
			System.out.println("The title matches");
			breturn = true;
		}
		else {
			System.out.println("Title did not match");
		}
		return breturn;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		if(ele.getText().equals(expectedText)) {
			System.out.println("The text "+expectedText+ " matches");
		}
		else {
			System.out.println("The text did not match");
		}
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		if(ele.getText().contains(expectedText)) {
			System.out.println("The text"+expectedText+ "matches");
		}
		else {
			System.out.println("The text did not match");
		}

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		if (ele.getAttribute(attribute).equals(value)) {
			System.out.println("Value matches");
		}
		else {
			System.out.println("Text not match");
		}

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		if (ele.getAttribute(attribute).contains(value)) {
			System.out.println("Value matches");
		}
		else {
			System.out.println("Text not match");
		}

	}

	@Override
	public void verifySelected(WebElement ele) {
		if (ele.isSelected()) {
			System.out.println("The "+ele+" is selected");
		}
		else {
			System.out.println("The "+ele+" is not selected");
		}

		takeSnap();
	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		if (ele.isDisplayed()) {
			System.out.println("The "+ele+" is displayed");
		}
		else {
			System.out.println("The "+ele+" is not displayed");
		}

		takeSnap();

	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> Allwindows = driver.getWindowHandles();
			List<String> list = new ArrayList<>();
			list.addAll(Allwindows);
			driver.switchTo().window(list.get(index));
			System.out.println("The control switched to new window of index: "+ index);
		} catch (NoSuchWindowException e) {
			System.out.println("The mentioned window as per the index:" +index+"is not available");

		}
		takeSnap();
	}

	@Override
	public void switchToFrame(WebElement ele) {

		try {
			driver.switchTo().frame(ele);
			System.out.println("Control moved successfully to the frame");
		} catch (NoSuchFrameException e) {
			System.out.println("Frame not available as per the identified webelement");

		}
		takeSnap();
	}

	public void switchToFramebyname(String frameName) {
		try {
			driver.switchTo().frame(frameName);
			System.out.println("Control moved successfully to the frame");
		} catch (NoSuchFrameException e) {
			System.out.println("Frame not available as per the identified webelement");

		}
		takeSnap();
		
	}

	public void switchToFramebyid(Integer fIndex) {
		try {
			driver.switchTo().frame(fIndex);
			System.out.println("Control moved successfully to the frame");
		} catch (NoSuchFrameException e) {
			System.out.println("Frame not available as per the identified webelement");

		}
		takeSnap();
		
	}

	@Override
	public void acceptAlert() {
		try {
			driver.switchTo().alert().accept();
			System.out.println("Alert accepted");
		} catch (NoAlertPresentException e) {
			System.out.println("Alert not present");

		}

	}

	@Override
	public void dismissAlert() {
		try {
			driver.switchTo().alert().dismiss();
			System.out.println("Alert dismissed");
		} catch (NoAlertPresentException e) {
			System.out.println("Alert not present");

		}

	}

	@Override
	public String getAlertText() {

		return null;
	}

	@Override
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dec = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, dec);
		} catch (IOException e) {

		}
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();

	}

	@Override
	public void closeAllBrowsers() {
		driver.quit();

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clickWithOutSnap(WebElement ele) {
		// TODO Auto-generated method stub
		
	}

	/*	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub

	}*/
	public List<WebElement> locateElements(String locator, String locValue) {
		try {
			switch (locator) {
			case "id": return driver.findElementsById(locValue);
			case "name": return driver.findElementsByName(locValue);	
			case "xpath": return driver.findElementsByXPath(locValue);
			case "class": return driver.findElementsByClassName(locValue);
			case "linktext": return driver.findElementsByLinkText(locValue);
			case "CssSelector": return driver.findElementsByCssSelector(locValue);
			case "PartialLinkText":return driver.findElementsByPartialLinkText(locValue);
			case "TagName": return driver.findElementsByTagName(locValue);

			}
		} catch (NoSuchElementException e) {
			System.out.println("The element is not found");
		} catch (WebDriverException e) {
			System.out.println("Unknown Exception occured");
		} 
		return null;
	}

public void esc() {
	driver.getKeyboard().sendKeys(Keys.ESCAPE);
	
}

}
